<div align="center">
  <img src=".gitlab/readme_assets/logo.png" alt="Logo" width="200">


  ### A Search and Exploration Tool for Semantic Data.
</div>

<br>
<br>
<br>

## About the Project
Online searches have become an integral part of everyday life, whether for personal interest in the latest sports or election results, or at work for solving problems. Companies like Microsoft and Google offer search engines that rely on a simple and clearly structured user interface. Users can use them intuitively to search for keywords or phrases. A list of hyperlinks is generated, which users then have to search through individually to find the desired information. However, it is often the case that users do not search for a website, but rather for data. This is not possible with search engines such as Google and Bing, where documents are analyzed and returned in order of importance. Boogle is a search and exploration tool for semantic data. The focus is on a self-explanatory interface with a simple text input field like in known document search engines. Users can perform fulltext queries as well as SPARQL queries. The results of the query are presented in a table. In addition, users can add more criteria to further refine the desired result, or expand it again by removing it. Users then can visualize the results of a query, and display the information as a graph.



### Sub Modules
The project is divided into Git-Submodules. Each submodule represents a micro service.

- **API**: The API service is the public gateway to the backend that delivers data to the frontend components.
- **Auth**: The Auth service manages user registration and logins. It includes authentication via OAuth2.
- **Discovery**: The Discovery service distributes traffic from the Api gateway to the correct backend service and manages load balancing.
- **Discussion**: The Discussion service manages the discussions about queries.
- **Frontend**: The Frontend is the client component of Boogle. It uses the JS frontend framework Vue.js.
- **Logging**: The Logging service logs events in the micro services and generates usage data with InfluxDB.
- **Query**: The Query service manages all incoming queries, stored user queries and prefixes that are used in queries.
- **Search**: The Search service manages full text queries sent to the local Wikidata backup.
- **User**: The User service stores and manages all user data.
- **Visualisation**: The Visualization service generates costly data visualizations.



### Technologies
- **Vue.js**: Frontend
- **Zuul**: Api
- **Eureka**: Discovery
- **Spring**: Api, Auth, Discovery, Discussion, Logging, Query, User, Visualisation
- **MariaDB**: Auth, Discussion, User
- **MongoDB**: Query
- **JWT & OAuth2**: Auth
- **InfluxDB**: Logging
- **Docker**: Api, Auth, Discovery, Discussion, Frontend, Logging, Query, User, Visualisation




## Getting Started

### Prerequisites

Boogle uses Docker to run the micro-services in isolated containers. The deployment requires only Docker Engine and Docker Compose.

### Configuration

Most of the configuration is done in the docker-compose.yml file. This is where the port mappings of the containers can be set and, if necessary, host directories can be mounted into the containers.



Some important environmental variables that **can** be set for the containers are:

**db-auth**

- `MYSQL_ROOT_PASSWORD`: The password of the auth database
- `MYSQL_DATABASE`: The name of the auth database
- `MYSQL_TCP_PORT`: The port of the auth database

**db-user**

- `MYSQL_ROOT_PASSWORD`: The password of the user database
- `MYSQL_DATABASE`: The name of the user database
- `MYSQL_TCP_PORT`: The port of the user database

**db-logging**

- `INFLUXDB_DB`: The name of the logging databse
- `INFLUXDB_ADMIN_USER`: The name of the admin user of the logging database
- `INFLUXDB_ADMIN_PASSWORD`: The password of the admin user of the logging database
- `INFLUXDB_HTTP_SHARED_SECRET`: The shared secret to validate requests to the logging database

**db-discussion**

- `MYSQL_ROOT_PASSWORD`: The password of the discussion database
- `MYSQL_DATABASE`: The name of the discussion database
- `MYSQL_TCP_PORT`: The port of the discussion database

**db-elastic**

- `node.name`: Name of the Elasticsearch instance
- `ELASTIC_PASSWORD`: The passwort of the user
- `ELASTIC_USERNAME`: The name of the user

**service-api**

- `ribbon.ReadTimeout`: The read timeout for gateway to service communication
- `server.port`: The port of the service

**service-auth**

- `spring.datasource.url`: The URL of the auth database
- `spring.datasource.username`: The name of the auth database user
- `spring.datasource.password`: The password of the auth database user 
- `spring.security.oauth2.client.registration.github.client-id`: The Github oauth client id 
- `spring.security.oauth2.client.registration.github.client-secret`: The Github oauth client secret
- `server.port`: The port of the service

**service-logging**

- `log.database.url`: The URL of the logging database
- `log.database.username`: The name of the logging database user
- `log.database.password`: The password of the logging database user
- `server.port`: The port of the service

**service-query**

- `spring.data.mongodb.port`: The port of the query database
- `spring.data.mongodb.database`: The name of the query database
- `spring.data.mongodb.host`: The hostname of the query database
- `database.init`: Enable / disable initialisation of the database with dummy queries
- `server.port`: The port of the service

**service-user**

- `spring.datasource.url`: The URL of the user database
- `spring.datasource.username`: The name of the user database user
- `spring.datasource.password`: The password of the user database user 
- `server.port`: The port of the service

**service-discussion**

- `pring.datasource.url`: The URL of the discussion database
- `spring.datasource.username`: The name of the discussion database user
- `spring.datasource.password`: The password of the discussion database user
- `spring.servlet.multipart.file-size-threshold`: The threshold for writing data to disc 
- `spring.servlet.multipart.max-file-size`: The maximum size for file upload
- `spring.servlet.multipart.max-request-size`: The maximum size for requests
- `server.port`: The port of the service

**service-elastic**

- `ELASTICSEARCH_CLUSTER`: The URL of the elastic database
- `ELASTICSEARCH_USER`: The name of the elastic database user
- `ELASTICSEARCH_PASSWORD`: The password of the elastic database user



To deploy Boogle to a server with TLS encryption, a certificate and a private key is required. Place the private key at `Nginx/nginx-selfsigned.key` and the certificate at `Nginx/nginx-selfsigned.crt`.



To deploy Boogle on a VM other than ep-2020-3.dimis.fim.uni-passau.de or locally, please consider the following configuration:

- **Frontend:** change axios base url in `plugins/axios.js` to `baseURL: process.env.baseURL || process.env.apiUrl || "http://localhost:8762"` (localhost for local deployment)
- **Frontend:** change url in `components/Login.vue` to `href="http://localhost:8762/service-auth/oauthlogin"` (localhost for local deployment)
- **API:** change cors config in `ZuulGatewayApplication.java` to ` config.addAllowedOrigin("http://localhost:8080")`  (localhost for local deployment)
- **Boogle** publish port `8762` of `service-api` in `docker-compose.yml` for local deployment
- Do not start the container `service-nginx` for local deployed 

### Data Persistence

The data of the containers is persisted using volumes that are managed by docker. If required, so-called bind mounts can be used and specified in the docker-compose.yml file. This can result in the configuration depending on the file system of the host.



The containers persist data in the following named volumes:

**db-auth**

- `db-auth-data`

**db-user**

- `db-user-data`

**db-logging**

- `db-logging-data`

**db-query**

- `db-query-data`

**db-discussion**

- `db-discussion-data`

**db-elastic**

- `db-search-data`

### Installation and Start Up

To install Boogle, checkout the latest tag from [tags](https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/boogle/-/tags) first. Then update the git submodules using `git submodule update`. 

Navigate to the directory where the docker-compose.yml file is located. Run the command `docker-compose build --no-cache --parallel`. After all images have been built, run the command `docker-compose up -d` to start the containers in detached mode.

## License
The project (including all submodules) is distributed under the GNU GLP License. See LICENSE for more information.


