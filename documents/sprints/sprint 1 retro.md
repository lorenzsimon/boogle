# Sprint 1 Retrospektive

**Gruppe 1**
- Johannes Böhm, 81214 (Sprintverantwortlicher)
- Julian Einwanger, 81889
- Christoph Knödlseder, 89899
- Thomas Desch, 82514
- Kathrin Holzinger, 79633
- Lorenz Simon, 85261

Dauer des Sprints: 07.11.2020 - 20.11.2020

## Was war das Ziel des Sprints?
Mehrere Ziele:
- Das Projekt aufzusetzen
- Sich in die Technologien einzulesen und einzuarbeiten 
- Das Konzept der Systemarchitektur zu bauen um zu zeigen, dass es funktioniert
- "Durchstich" vom Frontend zur Wikidata API und zurück

## Was wurde in dem Sprint erreicht?
- Gewöhnung an die Scrumpraktiken wie Dailyscrum
- Aufsetzen des Projekts
  - Dockercontainer und Dockercompose
  - Zuul mit Eureka
  - Query Service
  - Vue mit Vuetify, Vuex, Jest und Axios
- Einarbeiten in die Technologien
- Schicken einer Anfrage vom Frontend an Wikidata und Rendern der Ergebnisse

## Was lief gut während des Sprints?
- Motivation war hoch, alle interessiert und fleißig
- Kommunikation war gut, alle wussten was jeder macht
- Wenn einer nicht weiterkam konnte immer jemand helfen
- Issue Beschreibung war ausführlich und einheitlich

## Was lief schlecht während des Sprints?
- Einschätzen des Zeitaufwands von Issues (weight)
- Gleichmäßiges Verteilen der Aufgaben im Team gestaltete sich anfangs schwierig

## Starts:
- Aufteilen des Projektes in Submodules zur besseren Übersicht
- Issue Verwaltung Submodul zugehörig
- Epics verwenden um User Stories umzusetzen
- Dokumentation der Meetings
- Issues schneller reviewen, nicht erst am Schluss alles schließen (Burndownchart)
- Kleine Merge Requests mit nur einem Feature
- Klare Zuweisung von Verantwortlichkeiten bei Issues und MR

## Stops:
- Große Merge Requests

## Continue:
- Dailyscrum in Textform
- Tägliches abendliches Treffen und Rücksprache in Discord
